const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const app = express();
const port = 3000;

// Middleware pour parser le corps des requêtes en JSON
app.use(bodyParser.json());

// clé secret pour JWT
const SECRET_KEY = 'clé_super_secrete';

// Route de login
app.post('/login', (req, res) => {
    try{
        const { username, password } = req.body;
        // verification du payload
        if (!username || !password) {
            return res.status(400).json({ message: 'Payload mal formé' });
        }

        // verification du bon username/password
        if (username === 'user' && password === 'pass') {
            const payload = {
                username: username
            };
    
            // Génération du token JWT
            const token = jwt.sign(payload, SECRET_KEY, { expiresIn: '1h' });
    
            res.status(200).json({ 
                message: 'Connexion réussie',
                token: token 
            });
        } else {
            res.status(401).json({ message: 'Identifiants invalides' });
        }
    }catch(error){
        res.status(500).json({ message: 'Erreur serveur' });
    }
});

// Démarrer le serveur que si on execute le fichier app.js
if (require.main === module) {
    app.listen(port, () => {
        console.log(`Serveur démarré sur le port ${port}`);
    });
}

// On export l'app pour les tests
module.exports = app;
