const request = require('supertest');
const app = require('./app');
const assert = require('assert');

describe('POST /login', () => {
    it('Doit renvoyer le token de connexion (Connexion réussie)', (done) => {
        request(app)
            .post('/login')
            .send({ username: 'user', password: 'pass' })
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                assert(res.body.token, 'Pas de token');
                assert.strictEqual(res.body.message, 'Connexion réussie');
                done();
            });
    });

    it('Doit renvoyer 401 Unauthorized (Identifiants invalides)', (done) => {
        request(app)
            .post('/login')
            .send({ username: 'user', password: 'mauvaismdp' })
            .expect(401)
            .end((err, res) => {
                if (err) return done(err);
                assert.strictEqual(res.body.message, 'Identifiants invalides');
                done();
            });
    });

    it('Doit renvoyer 400 Bad Request (payload mal formé)', (done) => {
        request(app)
            .post('/login')
            .send({ username: 'user' })
            .expect(400)
            .end((err, res) => {
                if (err) return done(err);
                assert.strictEqual(res.body.message, 'Payload mal formé');
                done();
            });
    });
});
