const { Given, When, Then } = require('@cucumber/cucumber');
const request = require('supertest');
const assert = require('assert');
const app = require('../app');

let response;
let credentials;

Given('User provides valid credentials', function () {
  credentials = { username: 'user', password: 'pass' };
});

Given('User provides invalid credentials', function () {
  credentials = { username: 'user', password: 'wrongpass' };
});

When('User submits the login request', async function () {
  response = await request(app).post('/login').send(credentials);
});

Then('User should receive a success response with a token', function () {
  assert.strictEqual(response.status, 200);
  assert(response.body.token, 'No token received');
  assert.strictEqual(response.body.message, 'Connexion réussie');
});

Then('User should receive an unauthorized response', function () {
  assert.strictEqual(response.status, 401);
  assert.strictEqual(response.body.message, 'Identifiants invalides');
});
