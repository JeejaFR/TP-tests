Feature: User Login

Scenario: Successful login with valid credentials
  Given User provides valid credentials
  When User submits the login request
  Then User should receive a success response with a token

Scenario: Failed login with invalid credentials
  Given User provides invalid credentials
  When User submits the login request
  Then User should receive an unauthorized response
