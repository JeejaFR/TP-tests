const calculateBudget = require('./calculateBudget');

describe('calculateBudget', () => {
  // Calcul correct du budget avec des entrées valides
  test('Doit renvoyer le revenu moins les dépenses', () => {
      const income = 3000;
      const expenses = [1000, 200, 300];
      const expectedBudget = 1500;

      const calculateBudgetValue = calculateBudget(income, expenses);
      expect(calculateBudgetValue).toBe(expectedBudget);
  });
  // Gestion des erreurs lorsque les dépenses ne sont pas un tableau
  test('Doit renvoyer une erreur, Expenses must be an array', () => {
    const income = 3000;
    const expenses = 1200;

    expect(() => calculateBudget(income, expenses)).toThrow("Expenses must be an array");
  });

  // cas tableau vide
  test('Doit prendre en compte un tableau vide', () => {
      const income = 3000;
      const expenses = [];
      
      const calculateBudgetValue = calculateBudget(income, expenses);
      expect(calculateBudgetValue).toBe(income);
  });

  // cas depense negative 
  test('Doit prendre en compte les depenses negatives', () => {
    const income = 3000;
    const expenses = [-1000, 200, -300];
    const expectedBudget = 4100;

    const calculateBudgetValue = calculateBudget(income, expenses);
    expect(calculateBudgetValue).toBe(expectedBudget);
  });

  // cas revenu negatif
  test('Doit prendre en compte les revenus negatifs', () => {
    const income = -3000;
    const expenses = [1000, 200, 300];
    const expectedBudget = -4500;

    const calculateBudgetValue = calculateBudget(income, expenses);
    expect(calculateBudgetValue).toBe(expectedBudget);
  });

  // cas revenu negatif et depense negative
  test('Doit prendre en compte les revenus negatifs et les depenses negatives', () => {
    const income = -3000;
    const expenses = [-1000, 200, -300];
    const expectedBudget = -1900;

    const calculateBudgetValue = calculateBudget(income, expenses);
    expect(calculateBudgetValue).toBe(expectedBudget);
  });

});