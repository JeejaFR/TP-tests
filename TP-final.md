# TP Évaluation Finale

Jérémy DE FUENTES

## Partie 1 : Tests Unitaires (20 points)

### Objectif

Écrire des tests unitaires pour une fonction donnée.

### Le fichier : `calculateBudget.js` :

```javascript
function calculateBudget(income, expenses) {
  if (!Array.isArray(expenses)) throw new Error("Expenses must be an array");
  const totalExpenses = expenses.reduce((acc, expense) => acc + expense, 0);
  return income - totalExpenses;
}

module.exports = calculateBudget;
```
### Le fichier `calculateBudget.test.js` : 

```javascript
const calculateBudget = require('./calculateBudget');

describe('calculateBudget', () => {
  // Calcul correct du budget avec des entrées valides
  test('Doit renvoyer le revenu moins les dépenses', () => {
      const income = 3000;
      const expenses = [1000, 200, 300];
      const expectedBudget = 1500;

      const calculateBudgetValue = calculateBudget(income, expenses);
      expect(calculateBudgetValue).toBe(expectedBudget);
  });
  // Gestion des erreurs lorsque les dépenses ne sont pas un tableau
  test('Doit renvoyer une erreur, Expenses must be an array', () => {
    const income = 3000;
    const expenses = 1200;

    expect(() => calculateBudget(income, expenses)).toThrow("Expenses must be an array");
  });

  // cas tableau vide
  test('Doit prendre en compte un tableau vide', () => {
      const income = 3000;
      const expenses = [];
      
      const calculateBudgetValue = calculateBudget(income, expenses);
      expect(calculateBudgetValue).toBe(income);
  });

  // cas depense negative 
  test('Doit prendre en compte les depenses negatives', () => {
    const income = 3000;
    const expenses = [-1000, 200, -300];
    const expectedBudget = 4100;

    const calculateBudgetValue = calculateBudget(income, expenses);
    expect(calculateBudgetValue).toBe(expectedBudget);
  });

  // cas revenu negatif
  test('Doit prendre en compte les revenus negatifs', () => {
    const income = -3000;
    const expenses = [1000, 200, 300];
    const expectedBudget = -4500;

    const calculateBudgetValue = calculateBudget(income, expenses);
    expect(calculateBudgetValue).toBe(expectedBudget);
  });

  // cas revenu negatif et depense negative
  test('Doit prendre en compte les revenus negatifs et les depenses negatives', () => {
    const income = -3000;
    const expenses = [-1000, 200, -300];
    const expectedBudget = -1900;

    const calculateBudgetValue = calculateBudget(income, expenses);
    expect(calculateBudgetValue).toBe(expectedBudget);
  });

});
```

### Le fichier `package.json` : 

```json
{
  "devDependencies": {
    "jest": "^29.7.0"
  },
  "scripts": {
    "test": "jest"
  }
}
```

## Partie 2 : Tests Fonctionnels (25 points)

### Objectif

Écrire des tests fonctionnels pour une API REST.

### Le fichier : `app.js` :

```javascript
const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const app = express();
const port = 3000;

// Middleware pour parser le corps des requêtes en JSON
app.use(bodyParser.json());

// clé secret pour JWT
const SECRET_KEY = 'clé_super_secrete';

// Route de login
app.post('/login', (req, res) => {
    try{
        const { username, password } = req.body;
        // verification du payload
        if (!username || !password) {
            return res.status(400).json({ message: 'Payload mal formé' });
        }

        // verification du bon username/password
        if (username === 'user' && password === 'pass') {
            const payload = {
                username: username
            };
    
            // Génération du token JWT
            const token = jwt.sign(payload, SECRET_KEY, { expiresIn: '1h' });
    
            res.status(200).json({ 
                message: 'Connexion réussie',
                token: token 
            });
        } else {
            res.status(401).json({ message: 'Identifiants invalides' });
        }
    }catch(error){
        res.status(500).json({ message: 'Erreur serveur' });
    }
});

// Démarrer le serveur que si on execute le fichier app.js
if (require.main === module) {
    app.listen(port, () => {
        console.log(`Serveur démarré sur le port ${port}`);
    });
}

// On export l'app pour les tests
module.exports = app;
```

### Le fichier `package.json` : 

```json
{
  "name": "tp-final",
  "version": "1.0.0",
  "description": "",
  "main": "login.test.js",
  "scripts": {
    "test": "npx mocha login.test.js"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "body-parser": "^1.20.2",
    "express": "^4.19.2",
    "jsonwebtoken": "^9.0.2",
    "mocha": "^10.4.0",
    "supertest": "^7.0.0"
  }
}
```

### Le fichier `login.test.js` : 

```javascript
  const request = require('supertest');
  const app = require('./app');
  const assert = require('assert');

  describe('POST /login', () => {
    it('Doit renvoyer le token de connexion (Connexion réussie)', (done) => {
        request(app)
            .post('/login')
            .send({ username: 'user', password: 'pass' })
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                assert(res.body.token, 'Pas de token');
                assert.strictEqual(res.body.message, 'Connexion réussie');
                done();
            });
    });

    it('Doit renvoyer 401 Unauthorized (Identifiants invalides)', (done) => {
        request(app)
            .post('/login')
            .send({ username: 'user', password: 'mauvaismdp' })
            .expect(401)
            .end((err, res) => {
                if (err) return done(err);
                assert.strictEqual(res.body.message, 'Identifiants invalides');
                done();
            });
    });

    it('Doit renvoyer 400 Bad Request (payload mal formé)', (done) => {
        request(app)
            .post('/login')
            .send({ username: 'user' })
            .expect(400)
            .end((err, res) => {
                if (err) return done(err);
                assert.strictEqual(res.body.message, 'Payload mal formé');
                done();
            });
    });
  });

```

## Partie 3 : BDD avec Gherkin et Cucumber 40 points

### Objectif

Écrire des spécifications BDD et les implémenter avec Cucumber.

### Le fichier : `app.js` :

```javascript
  const express = require('express');
  const bodyParser = require('body-parser');
  const jwt = require('jsonwebtoken');
  const app = express();
  const port = 3000;

  // Middleware pour parser le corps des requêtes en JSON
  app.use(bodyParser.json());

  // clé secret pour JWT
  const SECRET_KEY = 'clé_super_secrete';

  // Route de login
  app.post('/login', (req, res) => {
      try{
          const { username, password } = req.body;
          // verification du payload
          if (!username || !password) {
              return res.status(400).json({ message: 'Payload mal formé' });
          }

          // verification du bon username/password
          if (username === 'user' && password === 'pass') {
              const payload = {
                  username: username
              };
      
              // Génération du token JWT
              const token = jwt.sign(payload, SECRET_KEY, { expiresIn: '1h' });
      
              res.status(200).json({ 
                  message: 'Connexion réussie',
                  token: token 
              });
          } else {
              res.status(401).json({ message: 'Identifiants invalides' });
          }
      }catch(error){
          res.status(500).json({ message: 'Erreur serveur' });
      }
  });

  // Démarrer le serveur que si on execute le fichier app.js
  if (require.main === module) {
      app.listen(port, () => {
          console.log(`Serveur démarré sur le port ${port}`);
      });
  }

  // On export l'app pour les tests
  module.exports = app;

```

### Le fichier `package.json` : 

```json
  {
    "name": "tp-final",
    "version": "1.0.0",
    "description": "",
    "main": "login.test.js",
    "scripts": {
      "test": "npx cucumber-js --require ./step_definitions"
    },
    "keywords": [],
    "author": "",
    "license": "ISC",
    "dependencies": {
      "body-parser": "^1.20.2",
      "express": "^4.19.2",
      "jsonwebtoken": "^9.0.2",
      "mocha": "^10.4.0",
      "supertest": "^7.0.0"
    },
    "devDependencies": {
      "@cucumber/cucumber": "^10.8.0"
    }
  }

```

### Le fichier `login.feature` : 

```
  Feature: User Login

  Scenario: Successful login with valid credentials
    Given User provides valid credentials
    When User submits the login request
    Then User should receive a success response with a token

  Scenario: Failed login with invalid credentials
    Given User provides invalid credentials
    When User submits the login request
    Then User should receive an unauthorized response
```

### Le fichier `login.steps.js` :

```javascript
const { Given, When, Then } = require('@cucumber/cucumber');
const request = require('supertest');
const assert = require('assert');
const app = require('../app');

let response;
let credentials;

Given('User provides valid credentials', function () {
  credentials = { username: 'user', password: 'pass' };
});

Given('User provides invalid credentials', function () {
  credentials = { username: 'user', password: 'wrongpass' };
});

When('User submits the login request', async function () {
  response = await request(app).post('/login').send(credentials);
});

Then('User should receive a success response with a token', function () {
  assert.strictEqual(response.status, 200);
  assert(response.body.token, 'No token received');
  assert.strictEqual(response.body.message, 'Connexion réussie');
});

Then('User should receive an unauthorized response', function () {
  assert.strictEqual(response.status, 401);
  assert.strictEqual(response.body.message, 'Identifiants invalides');
});

```

![alt text](<CleanShot 2024-06-18 at 10.43.53@2x.png>)

## Partie 4 : Intégration Continue avec GitLab-CI (15 points)

### Objectif

Configurer un pipeline CI simple pour exécuter les tests automatiquement.

### Le fichier `.gitlab-ci.yml` : 

```yml
image: node:21 

stages:
  - test

test_partie1:
  stage: test
  script:
    - cd Partie1
    - npm install
    - npm run test
  artifacts:
    paths:
      - Partie1/junit.xml
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
    - if: '$CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH =~ /^feature-/'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_COMMIT_BRANCH =~ /^hotfix-/'
    - if: '$CI_COMMIT_BRANCH =~ /^release-/'

test_partie2:
  stage: test
  script:
    - cd Partie2
    - npm install
    - npm run test
  artifacts:
    paths:
      - Partie2/junit.xml
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
    - if: '$CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH =~ /^feature-/'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_COMMIT_BRANCH =~ /^hotfix-/'
    - if: '$CI_COMMIT_BRANCH =~ /^release-/'

test_partie3:
  stage: test
  script:
    - cd Partie3
    - npm install
    - npm run test
  artifacts:
    paths:
      - Partie3/junit.xml
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
    - if: '$CI_COMMIT_BRANCH == "develop"'
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_TAG'
    - if: '$CI_COMMIT_BRANCH =~ /^feature-/'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    - if: '$CI_COMMIT_BRANCH =~ /^hotfix-/'
    - if: '$CI_COMMIT_BRANCH =~ /^release-/'

```

![alt text](<CleanShot 2024-06-18 at 10.45.22@2x.png>)

![alt text](<CleanShot 2024-06-18 at 10.45.41@2x.png>)

